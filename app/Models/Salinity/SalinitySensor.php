<?php

namespace App\Models\Salinity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SalinitySensor extends Model
{
    use HasFactory;

    public function insertData($salinity){
        $query = DB::table('salinity_sensors')->insert([
            'salinity_value' => $salinity,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        if(!$query){
            return false;
        }else{
            return true;
        }
    }

    public function allData(){
        return DB::table('salinity_sensors')->get();
    }

    public function latestData(){
        return DB::table('salinity_sensors')
            ->orderByDesc('id')->first();
    }
}
