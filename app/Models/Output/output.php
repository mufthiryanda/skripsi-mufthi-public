<?php

namespace App\Models\Output;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class output extends Model
{
    use HasFactory;

    public function insertData($selenoid,$garam,$artemia,$waterPump){
        $query = DB::table('outputs')->insert([
            'selenoid_status' => $selenoid,
            'servo_garam_status' => $garam,
            'servo_artemia_status' => $artemia,
            'water_pump_status' => $waterPump,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        if(!$query){
            return false;
        }else{
            return true;
        }
    }

    public function latestData(){
        return DB::table('outputs')
            ->orderByDesc('id')->first();
    }
}
