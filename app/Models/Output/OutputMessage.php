<?php

namespace App\Models\Output;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OutputMessage extends Model
{
    use HasFactory;

    public function insertData($msg,$status){
        $query = DB::table('output_messages')->insert([
           'msg' => $msg,
            'output' => $status,
        ]);

        if(!$query){
            return false;
        }else{
            return true;
        }
    }

    public function allData(){
        return DB::table('output_messages')->get();
    }

    public function latestDataSalt(){
        return DB::table('output_messages')
            ->where('output','=','salt')
            ->orderByDesc('id')
            ->limit(10)
            ->get();
    }

    public function latestDataSelenoid(){
        return DB::table('output_messages')
            ->where('output','=','selenoid')
            ->orderByDesc('id')
            ->limit(10)
            ->get();
    }

    public function latestDataWaterPump(){
        return DB::table('output_messages')
            ->where('output','=','water_pump')
            ->orderByDesc('id')
            ->limit(10)
            ->get();
    }

    public function latestDataArtemia(){
        return DB::table('output_messages')
            ->where('output','=','artemia')
            ->orderByDesc('id')
            ->limit(10)
            ->get();
    }
}
