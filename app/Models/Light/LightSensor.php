<?php

namespace App\Models\Light;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LightSensor extends Model
{
    use HasFactory;

    public function insertData($ldr,$intensity){
        $query = DB::table('light_sensors')->insert([
           'ldr_value' => $ldr,
           'intensity_value' =>$intensity,
           'created_at' => Carbon::now(),
           'updated_at' => Carbon::now(),
        ]);

        if(!$query){
            return false;
        }else{
            return true;
        }
    }

    public function allData(){
        return DB::table('light_sensors')->get();
    }

    public function latestData(){
        return DB::table('light_sensors')
                        ->orderByDesc('id')->first();
    }
}
