<?php

namespace App\Models\Temperature;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TemperatureSensor extends Model
{
    use HasFactory;

    public function insertData($temperature){
        $query = DB::table('temperature_sensors')->insert([
            'temperature_value' => $temperature,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        if(!$query){
            return false;
        }else{
            return true;
        }
    }

    public function allData(){
        return DB::table('temperature_sensors')->get();
    }

    public function latestData(){
        return DB::table('temperature_sensors')
            ->orderByDesc('id')->first();
    }
}
