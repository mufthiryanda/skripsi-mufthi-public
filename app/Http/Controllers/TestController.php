<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function index(Request $request){
        $test = $request->query('test');
        DB::table('temperature_sensors')->insert([
            'temperature_value' => $test,
            'created_at' => Carbon::now()
        ]);
        return response()->json([
           'status' => 200,
           'fill' => $test
        ]);
    }
}
