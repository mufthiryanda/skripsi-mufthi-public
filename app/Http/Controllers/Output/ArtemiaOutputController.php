<?php

namespace App\Http\Controllers\Output;

use App\Http\Controllers\Controller;
use App\Models\Output\output;
use App\Models\Output\OutputMessage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ArtemiaOutputController extends Controller
{
    public function index(){

        $query = new OutputMessage();
        $query2 = new output();
        $data = $query->latestDataArtemia();
        $data2 = $query2->latestData();

        return view('Output.ServoArtemia.index',[
            'title' => '-',
            'code' => 'output_artemia',
            'data' => $data,
            'data2' => $data2
        ]);
    }

    public function turnOn(){
        $query = DB::table('outputs')->update([
           'servo_artemia_status' => 'on',
            'updated_at' => Carbon::now(),
        ]);

        if(!$query){
            echo json_encode([
               'status' => 'failed',
                'msg' => 'Gagal menghidupkan servo artemia'
            ]);
        }else{
            echo json_encode([
               'status' => 'success',
               'msg' => 'Berhasil menghidupkan servo artemia'
            ]);
        }
    }

    public function turnOff(){
        $query = DB::table('outputs')->update([
            'servo_artemia_status' => 'off',
            'updated_at' => Carbon::now(),
        ]);

        if(!$query){
            echo json_encode([
                'status' => 'failed',
                'msg' => 'Gagal mematikan servo artemia'
            ]);
        }else{
            echo json_encode([
                'status' => 'success',
                'msg' => 'Berhasil mematikan servo artemia'
            ]);
        }
    }
}
