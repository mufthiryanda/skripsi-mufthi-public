<?php

namespace App\Http\Controllers\Output;

use App\Http\Controllers\Controller;
use App\Models\Output\output;
use App\Models\Output\OutputMessage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SalinityOutputController extends Controller
{
    public function index(){

        $query = new OutputMessage();
        $query2 = new output();
        $data = $query->latestDataSalt();
        $data2 = $query2->latestData();

        return view('Output.ServoSalinity.index',[
            'title' => '-',
            'code' => 'output_salinity',
            'data' => $data,
            'data2' => $data2,
        ]);
    }

    public function turnOn(){
        $query = DB::table('outputs')->update([
            'servo_garam_status' => 'on',
            'updated_at' => Carbon::now(),
        ]);

        if(!$query){
            echo json_encode([
                'status' => 'failed',
                'msg' => 'Gagal menghidupkan servo garam'
            ]);
        }else{
            echo json_encode([
                'status' => 'success',
                'msg' => 'Berhasil menghidupkan servo garam'
            ]);
        }
    }

    public function turnOff(){
        $query = DB::table('outputs')->update([
            'servo_garam_status' => 'off',
            'updated_at' => Carbon::now(),
        ]);

        if(!$query){
            echo json_encode([
                'status' => 'failed',
                'msg' => 'Gagal mematikan servo garam'
            ]);
        }else{
            echo json_encode([
                'status' => 'success',
                'msg' => 'Berhasil mematikan servo garam'
            ]);
        }
    }
}
