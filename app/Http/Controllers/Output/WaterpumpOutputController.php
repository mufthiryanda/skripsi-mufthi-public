<?php

namespace App\Http\Controllers\Output;

use App\Http\Controllers\Controller;
use App\Models\Output\output;
use App\Models\Output\OutputMessage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class WaterpumpOutputController extends Controller
{
    public function index(){

        $query = new OutputMessage();
        $query2 = new output();
        $data = $query->latestDataWaterPump();
        $data2 = $query2->latestData();

        return view('Output.WaterPump.index',[
            'title' => '-',
            'code' => 'output_water_pump',
            'data' => $data,
            'data2' => $data2,
        ]);
    }

    public function turnOn(){
        $query = DB::table('outputs')->update([
            'water_pump_status' => 'on',
            'updated_at' => Carbon::now(),
        ]);

        if(!$query){
            echo json_encode([
                'status' => 'failed',
                'msg' => 'Gagal menghidupkan servo artemia'
            ]);
        }else{
            echo json_encode([
                'status' => 'success',
                'msg' => 'Berhasil menghidupkan servo artemia'
            ]);
        }
    }

    public function turnOff(){
        $query = DB::table('outputs')->update([
            'water_pump_status' => 'off',
            'updated_at' => Carbon::now(),
        ]);

        if(!$query){
            echo json_encode([
                'status' => 'failed',
                'msg' => 'Gagal mematikan servo artemia'
            ]);
        }else{
            echo json_encode([
                'status' => 'success',
                'msg' => 'Berhasil mematikan servo artemia'
            ]);
        }
    }
}
