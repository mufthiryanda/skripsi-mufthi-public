<?php

namespace App\Http\Controllers\Sytem;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AllSystemController extends Controller
{
    public function index(){
        return view('AllSystem.index',[
           'title' => '-',
           'code' => 'all_system',
        ]);
    }

    public function allOutputStatus(){
        $query = DB::table('outputs')->first();
        return response()->json([
           'selenoid' => $query->selenoid_status,
            'water_pump' => $query->water_pump_status,
            'salinity' => $query->servo_garam_status,
            'artemia' => $query->servo_artemia_status
        ]);
    }
}
