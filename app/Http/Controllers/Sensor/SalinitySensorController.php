<?php

namespace App\Http\Controllers\Sensor;

use App\Http\Controllers\Controller;
use App\Models\Salinity\SalinitySensor;
use Illuminate\Http\Request;

class SalinitySensorController extends Controller
{
    public function index(){

        $query = new SalinitySensor();
        $data = $query->allData();
        $data2 = $query->latestData();

        return view('Sensor.Salinity.index',[
            'title' => '-',
            'code' => 'salinity_sensor',
            'data' => $data,
            'data2' => $data2,
        ]);
    }

    public function insertSalinitySensor(Request $request){
        $salinity_value = $request->query('salinity');

        $query = new SalinitySensor();
        $saveData = $query->insertData($salinity_value);

        if($saveData==true){
            return response()->json([
                'status' => 'success',
                'code' => '200',
                'msg' => 'Berhasil'
            ]);
        }else{
            return response()->json([
                'status' => 'failed',
                'code' => '404',
                'msg' => 'Gagal'
            ]);
        }
    }
}
