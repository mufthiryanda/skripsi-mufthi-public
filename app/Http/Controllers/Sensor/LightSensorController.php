<?php

namespace App\Http\Controllers\Sensor;

use App\Http\Controllers\Controller;
use App\Models\Light\LightSensor;
use Illuminate\Http\Request;

class LightSensorController extends Controller
{
    public function index(){

        $query = new LightSensor();
        $data = $query->allData();
        $data2 = $query->latestData();

        return view('Sensor.Light.index',[
            'title' => '-',
            'code' => 'light_sensor',
            'data' => $data,
            'data2' => $data2
        ]);
    }

    public function insertLightSensor(Request $request){
        $ldr_value = $request->query('ldr');
        $intensity_value = $request->query('intensity');

        $query = new LightSensor();
        $saveData = $query->insertData($ldr_value,$intensity_value);

        if($saveData==true){
            return response()->json([
               'status' => 'success',
               'code' => '200',
               'msg' => 'Berhasil'
            ]);
        }else{
            return response()->json([
                'status' => 'failed',
                'code' => '404',
                'msg' => 'Gagal'
            ]);
        }
    }
}
