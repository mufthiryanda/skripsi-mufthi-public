<?php

namespace App\Http\Controllers\Sensor;

use App\Http\Controllers\Controller;
use App\Models\Temperature\TemperatureSensor;
use Illuminate\Http\Request;

class TemperatureSensorController extends Controller
{
    public function index(){

        $query = new TemperatureSensor();
        $data = $query->allData();
        $data2 = $query->latestData();

        return view('Sensor.Temperature.index',[
            'title' => '-',
            'code' => 'temperature_sensor',
            'data' => $data,
            'data2' => $data2
        ]);
    }

    public function insertTemperatureSensor(Request $request){
        $temperature_value = $request->query('temperature');

        $query = new TemperatureSensor();
        $saveData = $query->insertData($temperature_value);

        if($saveData==true){
            return response()->json([
                'status' => 'success',
                'code' => '200',
                'msg' => 'Berhasil'
            ]);
        }else{
            return response()->json([
                'status' => 'failed',
                'code' => '404',
                'msg' => 'Gagal'
            ]);
        }
    }
}
