<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;

class HomeIndex extends Controller
{
    public function index(){
        return view('Homepage.index',[
            'title' => '',
            'code' => 'home'
        ]);
    }
}
