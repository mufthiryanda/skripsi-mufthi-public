<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[\App\Http\Controllers\Homepage\HomeIndex::class,'index'])->name('home');

Route::get('/all-system',[\App\Http\Controllers\Sytem\AllSystemController::class,'index'])->name('all-system');

Route::prefix('sensor')->group(function (){
    Route::get('/salinity',[\App\Http\Controllers\Sensor\SalinitySensorController::class,'index'])->name('salinity_sensor');
    Route::get('/light',[\App\Http\Controllers\Sensor\LightSensorController::class,'index'])->name('light_sensor');
    Route::get('/temperature',[\App\Http\Controllers\Sensor\TemperatureSensorController::class,'index'])->name('temperature_sensor');
});

Route::prefix('output')->group(function (){
    Route::get('/servo-artemia',[\App\Http\Controllers\Output\ArtemiaOutputController::class,'index'])->name('servo-artemia');
    Route::get('/servo-artemia/turn-on',[\App\Http\Controllers\Output\ArtemiaOutputController::class,'turnOn'])->name('turn-on-artemia');
    Route::get('/servo-artemia/turn-off',[\App\Http\Controllers\Output\ArtemiaOutputController::class,'turnOff'])->name('turn-off-artemia');
    Route::get('/servo-salinity',[\App\Http\Controllers\Output\SalinityOutputController::class,'index'])->name('servo-salinity');
    Route::get('/servo-salinity/turn-on',[\App\Http\Controllers\Output\SalinityOutputController::class,'turnOn'])->name('turn-on-salinity');
    Route::get('/servo-salinity/turn-off',[\App\Http\Controllers\Output\SalinityOutputController::class,'turnOff'])->name('turn-off-salinity');
    Route::get('/water-pump',[\App\Http\Controllers\Output\WaterpumpOutputController::class,'index'])->name('water-pump');
    Route::get('/water-pump/turn-on',[\App\Http\Controllers\Output\WaterpumpOutputController::class,'turnOn'])->name('turn-on-pump');
    Route::get('/water-pump/turn-off',[\App\Http\Controllers\Output\WaterpumpOutputController::class,'turnOff'])->name('turn-off-pump');
    Route::get('/selenoid-valve',[\App\Http\Controllers\Output\SelenoidValveController::class,'index'])->name('selenoid-valve');
    Route::get('/selenoid-valve/turn-on',[\App\Http\Controllers\Output\SelenoidValveController::class,'turnOn'])->name('turn-on-selenoid');
    Route::get('/selenoid-valve/turn-off',[\App\Http\Controllers\Output\SelenoidValveController::class,'turnOff'])->name('turn-off-selenoid');

    Route::get('/status',[\App\Http\Controllers\Sytem\AllSystemController::class,'allOutputStatus'])->name('output-status');
});

Route::prefix('api')->group(function (){
    Route::get('/light-sensor',[\App\Http\Controllers\Sensor\LightSensorController::class,'insertLightSensor'])->name('light-sensor-api');
    Route::get('/salinity-sensor',[\App\Http\Controllers\Sensor\SalinitySensorController::class,'insertSalinitySensor'])->name('salinity-api');
    Route::get('/temperature-sensor',[\App\Http\Controllers\Sensor\TemperatureSensorController::class,'insertTemperatureSensor'])->name('temperature-api');
});

Route::prefix('test')->group(function (){
    Route::get('/',[\App\Http\Controllers\TestController::class,'index']);
});
