<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OutputSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('outputs')->insert([
           'selenoid_status' => 'on',
            'servo_garam_status' => 'on',
            'servo_artemia_status' => 'on',
            'water_pump_status' => 'on',
            'created_at' => Carbon::now(),
        ]);
    }
}
