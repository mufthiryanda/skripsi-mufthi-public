<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OutputMessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('output_messages')->insert([
           [
            'msg' => 'Selenoid telah diaktifkan',
            'output' => 'selenoid',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
           ],
            [
                'msg' => 'Selenoid telah dimatikan',
                'output' => 'selenoid',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'msg' => 'Servo Artemia telah diaktifkan',
                'output' => 'salt',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'msg' => 'Servo Artemia telah dimatikan',
                'output' => 'salt',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'msg' => 'Water Pump telah diaktifkan',
                'output' => 'selenoid',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'msg' => 'Water Pump telah dimatikan',
                'output' => 'water_pump',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'msg' => 'Servo Garam telah diaktifkan',
                'output' => 'salt',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'msg' => 'Servo Garam telah dimatikan',
                'output' => 'salt',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }
}
