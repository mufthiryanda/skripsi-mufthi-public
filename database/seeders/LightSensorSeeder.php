<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LightSensorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('light_sensors')->insert([
            [
               'ldr_value' => 100,
               'intensity_value' => 20,
               'created_at' => Carbon::now(),
               'updated_at' => Carbon::now(),
            ],
            [
                'ldr_value' => 110,
                'intensity_value' => 23,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'ldr_value' => 108,
                'intensity_value' => 21,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'ldr_value' => 109,
                'intensity_value' => 24,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'ldr_value' => 108,
                'intensity_value' => 28,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }
}
