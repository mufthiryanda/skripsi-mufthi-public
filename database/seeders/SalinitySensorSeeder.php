<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SalinitySensorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('salinity_sensors')->insert([
           [
             'salinity_value' => 208,
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
           ],
            [
                'salinity_value' => 148,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'salinity_value' => 128,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'salinity_value' => 228,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'salinity_value' => 108,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }
}
