@extends('index')

@section('content')

    <div class="col">
        <div class="card radius-15">
            <div class="card-body text-center">
                <div class="p-4 border radius-15">
                    <img src="{{asset('assets/images/prototipe/university2.png')}}" width="110" height="110" class="rounded-circle shadow" alt="">
                    <h5 class="mb-1 mt-4">Mufthi Ryanda Olii</h5>
                    <h6 class="mb-1 mt-1">0705182055</h6>
                    <h6 class="mb-1 mt-1">Fisika-2 | Semester 8 | Stambuk 2018</h6>
                    <h6 class="mb-4 mt-1">Fakultas Sains & Teknologi - Universitas Islam Negeri Sumatera Utara</h6>
                    <p class="mb-3">RANCANG BANGUN SISTEM OTOMASI PENETASAN BRINE SHRIMP (Artemia salina L.) SEBAGAI PAKAN BURAYAK IKAN DENGAN MIKROKONTROLER NODEMCU ESP8266 BERBASIS IoT</p>

                    <div class="d-grid"> <a href="#" class="btn btn-light radius-15">Mulai Aplikasi</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection()
