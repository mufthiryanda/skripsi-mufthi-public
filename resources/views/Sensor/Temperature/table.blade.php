
<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nilai Suhu</th>
                    <th>Waktu</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $d)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$d->temperature_value}}&#8451;</td>
                        <td>{{$d->created_at}}</td>
                    </tr>
                @endforeach

            </table>
        </div>
    </div>
</div>
