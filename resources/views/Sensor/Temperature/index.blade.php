@extends('index')

@section('content')
    <h6 class="mb-0 text-uppercase">Data Sensor Suhu</h6>
    <hr/>

    <div class="col d-flex">
        <div class="card radius-10 w-100 overflow-hidden">
            <div class="card-body">
                <div class="col">
                    <div class="mb-3">
                        <h5 class="mb-0">Nilai Suhu</h5>
                    </div>
                    <div class="card radius-10 bg-warning">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-dark">Nilai Suhu</p>
                                    <h4 class="my-1 text-dark">{{$data2->temperature_value}}&#8451;</h4>
                                    <p class="mb-0 font-13 text-dark"><i class="bx bxs-timer align-middle"></i> Terakhir di update pada {{$data2->created_at}}</p>
                                </div>
                                <div class="widgets-icons bg-white text-dark ms-auto"><i class='bx bxs-thermometer'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @include('Sensor.Temperature.table')
@endsection
