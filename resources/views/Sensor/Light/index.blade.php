@extends('index')

@section('content')

<h6 class="mb-0 text-uppercase">Data Sensor LDR & GY-302</h6>
<hr/>

<div class="col d-flex">
    <div class="card radius-10 w-100 overflow-hidden">
        <div class="card-body">
            <div class="col">
                <div class="mb-3">
                    <h5 class="mb-0">Nilai LDR & Intensitas Cahaya</h5>
                </div>
                <div class="card radius-10 bg-warning">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div>
                                <p class="mb-0 text-dark">Nilai LDR</p>
                                <h4 class="my-1 text-dark">{{$data2->ldr_value}}</h4>
                                <p class="mb-0 font-13 text-dark"><i class="bx bxs-timer align-middle"></i> Terakhir di update pada ({{$data2->created_at}})</p>
                            </div>
                            <div class="widgets-icons bg-white text-dark ms-auto"><i class='bx bx-cloud'></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card radius-10 bg-warning">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div>
                                <p class="mb-0 text-dark">Nilai Intensitas Cahaya</p>
                                <h4 class="my-1 text-dark">{{$data2->intensity_value}} Lux</h4>
                                <p class="mb-0 font-13 text-dark"><i class="bx bxs-timer align-middle"></i> Terakhir di update pada ({{$data2->created_at}})</p>
                            </div>
                            <div class="widgets-icons bg-white text-dark ms-auto"><i class='bx bx-sun'></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

{{--  Table  --}}
@include('Sensor.Light.table')
{{--  End Of Table  --}}
@endsection
