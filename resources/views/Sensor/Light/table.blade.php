
<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nilai LDR</th>
                    <th>Nilai Lux</th>
                    <th>Waktu</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $d)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$d->ldr_value}}</td>
                        <td>{{$d->intensity_value}} Lux</td>
                        <td>{{$d->created_at}}</td>
                    </tr>
                @endforeach

            </table>
        </div>
    </div>
</div>
