@extends('index')

@section('content')

    <h6 class="mb-0 text-uppercase">Selenoid Valve</h6>
    <hr/>

    <div class="col d-flex">
        <div class="card radius-10 w-100 overflow-hidden">
            <div class="card-body">
                <div class="col">
                    <div class="mb-3">
                        <h5 class="mb-0">Status Selenoid Valve</h5>
                    </div>
                    <div class="card radius-10 bg-warning">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-dark">Status Alat :</p>
                                    <h4 class="my-1 text-dark">{{\Illuminate\Support\Str::upper($data2->selenoid_status)}}</h4>
                                    <p class="mb-0 font-13 text-dark"><i class="bx bxs-timer align-middle"></i> Terakhir di update pada ({{$data2->created_at}})</p>
                                </div>
                                <div class="widgets-icons bg-white text-dark ms-auto"><i class='bx bxs-gas-pump'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-grid"> <a href="#" onclick="turnOn()" class="btn btn-light radius-15">Hidupkan Servo</a>
                        <div class="clearfix mb-2"></div>
                        <a href="#" onclick="turnOff()" class="btn btn-light radius-15">Matikan Servo</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col col-lg-4">
        <div class="card radius-10 shadow-none bg-transparent overflow-hidden">
            <div class="card-body">
                <div class="d-lg-flex align-items-center">
                    <div>
                        <h5 class="mb-0">Data aktivasi Selenoid Valve</h5>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="dashboard-top-countries mb-3 px-2">
                <ul class="list-group list-group-flush radius-10">
                    @foreach($data as $d)
                    <li class="list-group-item d-flex align-items-center radius-10 mb-2 shadow-sm">
                        <div class="d-flex align-items-center">
                            <div class="flex-grow-1 ms-2">
                                <h6 class="mb-0">{{$d->msg}}</h6>
                            </div>
                        </div>
                        <div class="ms-auto">{{$d->created_at}}</div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
