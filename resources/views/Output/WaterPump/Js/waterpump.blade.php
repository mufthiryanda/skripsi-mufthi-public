<script src="{{asset('assets/js/dashboard-analytics.js')}}"></script>

<script>
    new PerfectScrollbar('.dashboard-top-countries');
</script>

<script>

    function turnOnWaterPump(){
        $.ajax({
            url: '{{route('turn-on-pump')}}',
            method: 'GET',
            dataType: "json",
            contentType: false,
            cache: false,
            processData: false,
            success: function(res) {
                if (res.status == 'success') {
                    swal({
                        title: 'Sukses',
                        text: res.msg,
                        icon: "success",
                        closeOnClickOutside: false,
                        closeOnEsc: false
                    }).then((ok) => {
                        window.location.href = '{{route('water-pump')}}';
                    });
                } else {
                    swal({
                        title: 'Gagal',
                        text: res.msg,
                        icon: "error",
                        closeOnClickOutside: false,
                        closeOnEsc: false
                    });
                }
            }
        });
    }

    function turnOffWaterPump(){
        $.ajax({
            url: '{{route('turn-off-pump')}}',
            method: 'GET',
            dataType: "json",
            contentType: false,
            cache: false,
            processData: false,
            success: function(res) {
                if (res.status == 'success') {
                    swal({
                        title: 'Sukses',
                        text: res.msg,
                        icon: "success",
                        closeOnClickOutside: false,
                        closeOnEsc: false
                    }).then((ok) => {
                        window.location.href = '{{route('water-pump')}}';
                    });
                } else {
                    swal({
                        title: 'Gagal',
                        text: res.msg,
                        icon: "error",
                        closeOnClickOutside: false,
                        closeOnEsc: false
                    });
                }
            }
        });
    }

</script>
