
<script src="{{asset('assets/js/dashboard-analytics.js')}}"></script>

<script>
    new PerfectScrollbar('.dashboard-top-countries');
</script>

<script>
    function turnOnArtemia(){
        $.ajax({
            url: '{{route('turn-on-artemia')}}',
            method: 'GET',
            dataType: "json",
            contentType: false,
            cache: false,
            processData: false,
            success: function(res) {
                if (res.status == 'success') {
                    swal({
                        title: 'Sukses',
                        text: res.msg,
                        icon: "success",
                        closeOnClickOutside: false,
                        closeOnEsc: false
                    }).then((ok) => {
                        window.location.href = '{{route('servo-artemia')}}';
                    });
                } else {
                    swal({
                        title: 'Gagal',
                        text: res.msg,
                        icon: "error",
                        closeOnClickOutside: false,
                        closeOnEsc: false
                    });
                }
            }
        });
    }

    function turnOffArtemia(){
        $.ajax({
            url: '{{route('turn-off-artemia')}}',
            method: 'GET',
            dataType: "json",
            contentType: false,
            cache: false,
            processData: false,
            success: function(res) {
                if (res.status == 'success') {
                    swal({
                        title: 'Sukses',
                        text: res.msg,
                        icon: "success",
                        closeOnClickOutside: false,
                        closeOnEsc: false
                    }).then((ok) => {
                        window.location.href = '{{route('servo-artemia')}}';
                    });
                } else {
                    swal({
                        title: 'Gagal',
                        text: res.msg,
                        icon: "error",
                        closeOnClickOutside: false,
                        closeOnEsc: false
                    });
                }
            }
        });
    }
</script>
