<div class="sidebar-wrapper" data-simplebar="true">
    <div class="sidebar-header">
        <div>
            <img src="{{asset('assets/images/logo-icon.png')}}" class="logo-icon" alt="logo icon">
        </div>
        <div>
            <h4 class="logo-text">Mufthi</h4>
        </div>
        <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
        </div>
    </div>
    <!--navigation-->
    <ul class="metismenu" id="menu">
        <li>
            <a href="{{route('home')}}">
                <div class="parent-icon"><i class="bx bx-home-circle"></i>
                </div>
                <div class="menu-title">Home</div>
            </a>
        </li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-scan'></i>
                </div>
                <div class="menu-title">Sensor</div>
            </a>
            <ul>
                <li> <a href="{{route('salinity_sensor')}}"><i class="bx bx-right-arrow-alt"></i>Data Salinitas</a>
                </li>
                <li> <a href="{{route('temperature_sensor')}}"><i class="bx bx-right-arrow-alt"></i>Data Suhu</a>
                </li>
                <li> <a href="{{route('light_sensor')}}"><i class="bx bx-right-arrow-alt"></i>Data Cahaya</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-pulse'></i>
                </div>
                <div class="menu-title">Output</div>
            </a>
            <ul>
                <li> <a href="{{route('servo-salinity')}}"><i class="bx bx-right-arrow-alt"></i>Servo Garam</a>
                </li>
                <li> <a href="{{route('servo-artemia')}}"><i class="bx bx-right-arrow-alt"></i>Servo Artemia</a>
                </li>
                <li> <a href="{{route('selenoid-valve')}}"><i class="bx bx-right-arrow-alt"></i>Selenoid Valve</a>
                </li>
                <li> <a href="{{route('water-pump')}}"><i class="bx bx-right-arrow-alt"></i>Water Pump</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{route('all-system')}}">
                <div class="parent-icon"><i class="bx bx-desktop"></i>
                </div>
                <div class="menu-title">Seluruh Sistem</div>
            </a>
        </li>
    </ul>
    <!--end navigation-->
</div>
