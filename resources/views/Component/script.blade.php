<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<!--plugins-->
<script src="{{asset('assets/plugins/simplebar/js/simplebar.min.js')}}"></script>
<script src="{{asset('assets/plugins/metismenu/js/metisMenu.min.js')}}"></script>
<script src="{{asset('assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
<script src="{{asset('assets/plugins/chartjs/js/Chart.min.js')}}"></script>
<script src="{{asset('assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
<script src="{{asset('assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('assets/plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('assets/plugins/sparkline-charts/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-knob/excanvas.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-knob/jquery.knob.js')}}"></script>
<script>
    $(function() {
        $(".knob").knob();
    });
</script>
<script src="{{asset('assets/js/index.js')}}"></script>
<!--app JS-->
<script src="{{asset('assets/js/app.js')}}"></script>

@switch($code)

    @case('light_sensor')
    @include('Sensor.Light.Js.light')
    @break

    @case('salinity_sensor')
    @include('Sensor.Light.Js.light')
    @break

    @case('temperature_sensor')
    @include('Sensor.Temperature.Js.temperature')
    @break

    @case('selenoid_valve')
    @include('Output.SelenoidValve.Js.selenoid')

    @case('output_artemia')
    @include('Output.ServoArtemia.Js.artemia')

    @case('output_water_pump')
    @include('Output.WaterPump.Js.waterpump')

    @case('output_salinity')
    @include('Output.ServoSalinity.Js.salinity')

    @case('output_selenoid')
    @include('Output.SelenoidValve.Js.selenoid')

    @default
    <script></script>

@endswitch

