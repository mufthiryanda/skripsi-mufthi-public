<!doctype html>
<html lang="en">


<head>
    @include('Component.header')
</head>

<body class="bg-theme bg-theme1">
<!--wrapper-->
<div class="wrapper">

    <!--sidebar wrapper -->
@include('Component.sidebar')
<!--end sidebar wrapper -->

    <!--start header -->
    <header>
        @include('Component.topbar')
    </header>
    <!--end header -->
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">

            @yield('content')

        </div>
    </div>
    <!--end page wrapper -->
    <!--start overlay-->
    <div class="overlay toggle-icon"></div>
    <!--end overlay-->
    <!--Start Back To Top Button--> <a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
    <!--End Back To Top Button-->
    <footer class="page-footer">
        @include('Component.footer')
    </footer>
</div>
<!--end wrapper-->

<!-- Bootstrap JS -->
@include('Component.script')
</body>


<!-- Mirrored from codervent.com/dashtreme/demo/vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Jul 2022 15:21:53 GMT -->
</html>
